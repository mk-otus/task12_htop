# Otus_task12

Мониторинг производительности. Perf

Ставим atop на мониторинг каждые 5 секунд
```
yum install -y atop perf
sed 's/600/5/' /usr/share/atop/atop.daily -i
atop -r /var/log/atop/atop_20190920
```


В это время нагрузим проц

```
yum install -y stress
stress --cpu 1 --timeout 30s

```

Видим нгрузку через atop, но не видим от кого (stress %1) 
черещ top видим кто нагружает (stress 98%)